import * as React from "react"
import {Platform, StatusBar} from 'react-native'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Alert} from 'react-native'
import { Container, Header, Title, Content, Icon, Text, Button, Left, Body, Right, List, ListItem, View,} from "native-base"
import { Avatar } from 'react-native-elements'
import {deleteJob} from '../../../container/BlankPageContainer/action'
import styles from "./styles"

export interface Props {
  navigation: any;
  jobs: any;
}
export interface State {}

class Home extends React.Component<Props, State> {

  createAJob = () => {
    this.props.navigation.navigate("BlankPage", {
      name: "Create Listing"
    })
  }

  // now we have Id, see the delete button
  delete = (jobId) => {
    let target = jobId
    console.log(target)
		Alert.alert(
      'Do you want to delete this job?',
      '',
			[
			  {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
			  {text: 'OK', onPress: () => {
          //should put it here. only send delete action when you clicked OK!
          this.props.delete(jobId);
          }
        }
			],
      { cancelable: false },
      )
      // this.props.navigation.navigate("Home")
      // this.props.delete(this.state) 
      // this is wrong, because you delete action wants to get a id, not the state
      // and we can't put it here, because it will be delete no matter what choice you choose!
	}

  render() {
    return (
      <Container style={{backgroundColor: '#1b1616'}}>
        <Header style={{ marginTop: Platform.OS === 'ios' ? 12 : 30, backgroundColor: '#1b1616', borderBottomColor: 'black'}} >
        <View>
					<StatusBar
            translucent
            backgroundColor='black'
						barStyle="light-content"
					/>
				</View>
          <Left>
            <Button transparent>
              <Icon
                active
                style={{color: Platform.OS === 'ios' ? 'white' : 'white'}}
                name="menu"
                onPress={() => this.props.navigation.navigate("DrawerOpen")}
              />
              <Title style={{marginLeft: 20, color: "#FFFFFF"}}>Dashboard</Title>
            </Button>
          </Left>
          <Right>
            <Button transparent style={{marginBottom: 10}}>
              <Icon name='search' style={{marginRight: 20, color: '#9D1B33'}}/>
            </Button>
            <Button block Iconcenter onPress={this.createAJob} style={{padding: 20, width: 90, height: 37, marginTop: 7, backgroundColor: '#9D1B33', marginBottom: 15 }}>
							<Icon name='add' style={{color:'white', fontSize: 32}}/>
							{/* Image doesn't have onPress, check https://facebook.github.io/react-native/docs/image */}
						</Button>
          </Right>
        </Header>
        
        <Content>
          <List style={styles.list}>
          <ListItem itemDivider Thumbnail style={{backgroundColor: "#1b1616"}}>
            <Text style={{color: "#FFFFFF"}}>Open</Text>
          </ListItem>
            {this.props.jobs.map((item, i) => (
              <Content key={i}>
              <ListItem
                style={{ marginLeft: 0,borderBottomColor: '#1b1616', borderBottomWidth: 3}}
                onPress={() => this.delete(item.id)}
              >
              <Left style={{marginLeft: 10}}>
                <Avatar
                  medium
                  rounded
                  title={item.title.charAt(0)}
                  overlayContainerStyle={{backgroundColor: '#9d1b33'}}
                />
                <Body>
                  <Button rounded style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderRadius: 5, 
                        borderWidth: 2, 
                        borderColor: '#3CBE63',
                        backgroundColor: '#3CBE63',
                        height: 20,
                        margin: 0,
                        }}>
                    <Text 
                      note numberOfLines={1} 
                      style={{color: "black",
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                    >
                    {item.category}
                    </Text> 
                  </Button>
                  <Text numberOfLines={1} style={{margin: 0, color: "#FFFFFF", fontSize: 18}}>{item.title} </Text>  
                  <Text note numberOfLines={2}>
                  {item.description}
                  </Text>
                </Body>
              </Left>
              <Right>
                <Text style={{color: '#9D1B33', fontSize:13}}>{item.start}</Text>
                <Button 
                  transparent
                  // key !!!!!!!!!  because we still in the callback of the map function, 
                  // so we can access every item in map(see line 79.)   
                  // somearray.map(function(item, index){ ... })
                  // it's important to understand the common methods of array, e.g  map, filter, reduce, forEach and so on. Check MDN
                  onPress={() => this.delete(item.id)}
                >
                  <Icon name="arrow-forward" style={{color: '#9D1B33'}}></Icon>
                </Button>
              </Right>   
            </ListItem>
            </Content>
            ))}
            <ListItem itemDivider style={{backgroundColor: "#1b1616"}}>
              <Text style={{color: "#FFFFFF"}}>On Hold</Text>
            </ListItem>
            {this.props.jobs.map((item, i) => (
              <Content key={i}>
              <ListItem
                style={{ marginLeft: 0,borderBottomColor: '#1b1616', borderBottomWidth: 3}}
                onPress={() => this.delete(item.id)}
              >
              <Left style={{marginLeft: 10}}>
                <Avatar
                  medium
                  rounded
                  title={item.title.charAt(0)}
                  overlayContainerStyle={{backgroundColor: '#9d1b33'}}
                />
                <Body>
                  <Button rounded style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderRadius: 5, 
                        borderWidth: 2, 
                        borderColor: '#BE863C',
                        backgroundColor: '#BE863C',
                        height: 20,
                        margin: 0,
                        }}>
                    <Text 
                      note numberOfLines={1} 
                      style={{color: "black",
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                    >{item.category}
                    </Text> 
                  </Button>
                  <Text numberOfLines={1} style={{margin: 0, color: "#FFFFFF", fontSize: 18}}>{item.title}</Text>  
                  <Text note numberOfLines={2}>{item.description}</Text>
                </Body>
              </Left>
              <Right>
                <Text style={{color: '#9D1B33', fontSize:13}}>{item.start}</Text>
                <Button 
                  transparent
                  // key !!!!!!!!!  because we still in the callback of the map function, 
                  // so we can access every item in map(see line 79.)   
                  // somearray.map(function(item, index){ ... })
                  // it's important to understand the common methods of array, e.g  map, filter, reduce, forEach and so on. Check MDN
                  onPress={() => this.delete(item.id)}
                >
                  <Icon name="arrow-forward" style={{color: '#9D1B33'}}></Icon>
                </Button>
              </Right>   
            </ListItem>
            </Content>
            ))}
          </List>
        </Content>
      </Container>
    );
  }
}


Home = connect(mapStateToProps, mapDispatchToProps)(Home)

function mapDispatchToProps(dispatch) {
	return {delete: bindActionCreators(deleteJob, dispatch)}
}

function mapStateToProps (){
	return {
		test: '',
	}
}


export default Home;

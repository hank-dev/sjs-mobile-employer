import { StyleSheet } from "react-native";

const styles: any = StyleSheet.create({
	container: {
		backgroundColor: "#1b1616",
	},
	row: {
		backgroundColor: "#1b1616",
		flex: 1,
		alignItems: "center",
	},
	text: {
		color: "#9d1b33",
		fontSize: 20,
		marginBottom: 15,
		alignItems: "center",
	},
	mt: {
		marginTop: 18,
	},
	list: {
		backgroundColor: "#2E2E32",
	}
});
export default styles;

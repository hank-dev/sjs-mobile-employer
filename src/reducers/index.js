import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import { jobReducer } from "./reducer";

import homeReducer from "../container/HomeContainer/reducer";

export default combineReducers({
	form: formReducer,
	//homeReducer,
	jobs: jobReducer
});

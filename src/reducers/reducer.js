export function jobReducer(jobs = [], action) {
    //let newState = JSON.parse(JSON.stringify(state));
    switch (action.type) {
        case 'add':
            let newJob = action.payload;
            let clone_jobs = jobs.map(job => job)
            clone_jobs.push(newJob)

            return clone_jobs

        case 'delete':
            var jobTemp = jobs.slice(0, jobs.length) // expected output: jobTemp = all the jobs in data.josn
            var id = action.payload // expected output: id = the one we store in state
            jobArr = jobTemp.filter(function (job) { // get the jobs whihch match with the same id in the state then stroe in jobArr
                if (job.id == id)
                    return false;
                else
                    return true
            })
            console.log(jobArr)
            return jobArr // send newArr back to action

        default:
            return jobs;
    }
}
// @flow
import React from "react";
import { StackNavigator, DrawerNavigator } from "react-navigation";
import { Root } from "native-base";
import LoginContainer from "./container/LoginContainer";
import HomeContainer from "./container/HomeContainer";
import BlankPageContainer from "./container/BlankPageContainer";
import SidebarContainer from "./container/SidebarContainer";


const Drawer = DrawerNavigator(
	{
		Home: { screen: HomeContainer },
	},
	{
		initialRouteName: "Home",
		contentComponent: props => <SidebarContainer {...props} />,
	}
);

const App = StackNavigator(
	{
		Login: { screen: LoginContainer },//LoginContainer
		BlankPage: { screen: BlankPageContainer },
		Drawer: { screen: Drawer },
	},
	{
		initialRouteName: "Login",
		headerMode: "none",
	}
);

export default () => (
		<Root>
			<App />
		</Root>
);

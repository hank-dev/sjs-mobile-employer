 export function addJob(Job) {
     return function (dispatch) {
         dispatch({
             type: 'add',
             payload: Job
         })
     }
 }

 export function deleteJob(id) {
     return function (dispatch) {
        dispatch({
             type: 'delete',
             payload: id
         })
     }
 }
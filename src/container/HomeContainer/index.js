// @flow
import * as React from "react"
import { connect } from "react-redux"
import Home from "../../stories/screens/Home"
import { fetchList } from "./actions"

import { Container, TabHeading, Tab, Tabs, Text, Icon } from 'native-base'
import { Row, Grid } from 'react-native-easy-grid'

export interface Props {
	navigation: any,
	fetchList: Function,
	jobs: Object,
}

const center = {
	flex: 1,
	justifyContent: 'center',
};



export interface State {}
class HomeContainer extends React.Component<Props, State> {
	componentDidMount() {
		//this.props.fetchList(datas.data)
	}
	render() {
		return (
			<Container scrollEnabled={false}>
        		<Tabs scrollEnabled={false} tabBarPosition="bottom">
         			 <Tab heading={
						<TabHeading>
							<Grid>
								<Row style={center}><Icon name="home" /></Row>
								<Row style={center}><Text>Dashboard</Text></Row>
							</Grid>
						</TabHeading>}>
						<Home navigation={this.props.navigation} jobs={this.props.jobs} />
				</Tab>
         		<Tab  heading={
				  	<TabHeading>
						<Grid>
							<Row style={center}><Icon name="person" /></Row>
							<Row style={center}><Text>Profile</Text></Row>
						</Grid>
				</TabHeading>}>
					<Home navigation={this.props.navigation} jobs={this.props.jobs} />
         		 </Tab>
         		<Tab heading={
				 <TabHeading>
					<Grid>
						<Row style={center}><Icon name="settings" /></Row>
						<Row style={center}><Text>Settings</Text></Row>
					</Grid>
				</TabHeading>}>
					<Home navigation={this.props.navigation} jobs={this.props.jobs} />
				</Tab>
        		</Tabs>
     		 </Container>
		)
	}
}

function bindAction(dispatch) {
	return {
		fetchList: url => dispatch(fetchList(url)),
	}
}

const mapStateToProps = state => ({
	jobs: state.jobs,
	//isLoading: state.homeReducer.isLoading,
})

export default connect(mapStateToProps, bindAction)(HomeContainer);

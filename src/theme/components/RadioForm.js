import * as React from "react"
import { View } from 'react-native'
import RadioForm from 'react-native-simple-radio-button';

  
  class RadioButtonOption extends React.Component {

    render () {
      const {radio_props} = this.props

      return (
        <View>
          <RadioForm
            style={{height:30}}
            radio_props={radio_props}
            initial={-1}
            formHorizontal={true}
            labelHorizontal={true}
            selectedLabelColor={'white'}
            labelStyle={{marginRight: 30}}
            labelColor={'#B2B2B2'}
            selectedButtonColor={'#9D1B33'}
            buttonColor={'#9D1B33'}
            buttonSize={11}
            borderWidth={3}
            animation={true}
            buttonOuterSize={22}
            buttonInnerSize={30}
            onPress={(value) => {this.setState({value:value})}}
            buttonWrapStyle={{margin: 10}}
            wrapStyle={{margin: 20}}
          />
        </View>
      );
    }
  }

export default RadioButtonOption